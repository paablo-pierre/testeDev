package seap.testeDEV.Bean;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import seap.testeDEV.model.Pessoa;
import seap.testeDEV.model.Servidor;
import seap.testeDEV.model.Telefone;
import seap.testeDEV.model.TipoVinculo;
import seap.testeDEV.model.repository.PessoaRepository;
import seap.testeDEV.model.repository.ServidorRepository;
import seap.testeDEV.model.repository.TelefoneRepository;
import seap.testeDEV.model.repository.TipoVinculoRepository;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@Scope("view")
public class CadastrarPessoaBean implements Serializable {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private TelefoneRepository telefoneRepository;

    @Autowired
    private ServidorRepository servidorRepository;

    @Autowired
    private TipoVinculoRepository tipoVinculoRepository;

    private Pessoa pessoa;
    private Telefone telefone;
    private Servidor servidor;
    private Integer tipoVinculo;
    private String paginaRetorno;


    @PostConstruct
    public void init() {
        pessoa = new Pessoa();
        telefone = new Telefone();
        servidor = new Servidor();
    }

    public void salvar() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Salvo com sucesso"));

        salvarPessoa();
        salvarTelefone();
        salvarServidor();
    }

    // Inicio métodos para salvar os dados
    private void salvarPessoa() {
        if (pessoa != null) {
            try {
                pessoaRepository.save(pessoa);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void salvarTelefone() {
        if (telefone != null) {
            try {
                telefone.setPessoa(pessoa);
                telefoneRepository.save(telefone);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void salvarServidor() {
        if (!servidor.getMatricula().isEmpty()) {
            if (tipoVinculo != null && pessoa != null) {
                try {
                    servidor.setTipoVinculo(tipoVinculoRepository.findOne(tipoVinculo));
                    servidor.setPessoa(pessoa);
                    servidorRepository.save(servidor);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
    //Fim dos metodos para salvar dados

    //redireciona a página para pesquisa de servidor por nome
    public String redirect() {
        return paginas(paginaRetorno);
    }

    //redireciona a página para pesquisa de servidor por vinculo
    public String redirectVinculo() {
        return paginas(paginaRetorno);
    }

    //redireciona para a página inicial
    public String voltar() {
        return paginas(paginaRetorno);
    }

    //redireciona para a pagina de pesquisa personalizada
    public String procuraPersonalizada() {
        return paginas(paginaRetorno);
    }

    public String procuraPorId(){
        return paginas(paginaRetorno);
    }

    public String paginas(String rota) {
        HashMap<String, String> paginas = new HashMap<>();
        paginas.put("pesquisaServidor", "/pages/admin/pesquisaServidor.xhtml?faces-redirect=true");
        paginas.put("pesquisarVinculo","/pages/admin/pesquisarVinculo.xhtml?faces-redirect=true");
        paginas.put("principal", "/pages/admin/principal.xhtml?faces-redirect=true");
        paginas.put("pesquisaPersonalizada", "/pages/admin/pesquisaPersonalizada.xhtml?faces-redirect=true");
        paginas.put("pesquisaId", "/pages/admin/pesquisaId.xhtml?faces-redirect=true");
        return paginas.get(rota);
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor servidor) {
        this.servidor = servidor;
    }

    public Integer getTipoVinculo() {
        return tipoVinculo;
    }

    public void setTipoVinculo(Integer tipoVinculo) {
        this.tipoVinculo = tipoVinculo;
    }

    public String getPaginaRetorno() {
        return paginaRetorno;
    }

    public void setPaginaRetorno(String paginaRetorno) {
        this.paginaRetorno = paginaRetorno;
    }
}