package seap.testeDEV.Bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import seap.testeDEV.model.Pessoa;
import seap.testeDEV.model.Servidor;
import seap.testeDEV.model.Telefone;
import seap.testeDEV.model.TipoVinculo;
import seap.testeDEV.model.repository.PessoaRepository;
import seap.testeDEV.model.repository.ServidorRepository;
import seap.testeDEV.model.repository.TelefoneRepository;
import seap.testeDEV.model.repository.TipoVinculoRepository;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Controller
@Scope("view")
public class ConsultarServidorBean implements Serializable {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private TelefoneRepository telefoneRepository;

    @Autowired
    private ServidorRepository servidorRepository;

    @Autowired
    private TipoVinculoRepository tipoVinculoRepository;

    private Pessoa pessoa;
    private Telefone telefone;
    private Servidor servidor;
    private Integer tipoVinculo;
    private List<TipoVinculo> tipoVinculoList;
    private List<Servidor> servidorList;
    private List<Pessoa> pessoaList;

    @PostConstruct
    public void initialize() {
        pessoa = new Pessoa();
        telefone = new Telefone();
        servidor = new Servidor();
        tipoVinculoList = tipoVinculoRepository.findAll();
        pessoaList = new ArrayList<>();
        servidorList = new ArrayList<>();
    }

    /* Método para buscar a matricula e o Tipo
     * de Vinculo da pessoa cadastrada
     */
    public Servidor buscarServidor(Pessoa pessoa) {
        return servidorRepository.findFirstByPessoa(pessoa);
    }

    public List<Pessoa> buscar() {
        return pessoaList = pessoaRepository.findAllByNomeIsLike(pessoa.getNome());
    }

    //busca os cadastrados por nome através dos servidores. Retornando o nome
    public List<Servidor> buscarPorNome() {
        return servidorList = servidorRepository.findAllByPessoaNome(pessoa.getNome());
    }

    //busca os cadastrados por vinculo através de uma lista de servidores
    public List<Servidor> buscarPorVinculo() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Busca concluida com sucesso"));

        return servidorList = servidorRepository.findAllByTipoVinculo(new TipoVinculo(tipoVinculo));
    }

    //busca é feita por nome e vinculo ao mesmo tempo
    public List<Servidor> buscarNomeVinculo() {
        return servidorList = servidorRepository.findAllByPessoaNomeAndTipoVinculo(pessoa.getNome(), tipoVinculo);
    }

    public void buscaPersonalizada() {
        if (pessoa.getNome() != null) {
            buscarPorNome();
        }
        else if (tipoVinculo != null) {
            buscarPorVinculo();
        }
        if(pessoa.getNome() != null && tipoVinculo != null) {
            buscarNomeVinculo();
        }
    }

    /* Final dos métodos para buscar servidor
     * através de matricula e tipo de vinculo
     */
    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor servidor) {
        this.servidor = servidor;
    }

    public Integer getTipoVinculo() {
        return tipoVinculo;
    }

    public void setTipoVinculo(Integer tipoVinculo) {
        this.tipoVinculo = tipoVinculo;
    }

    public List<TipoVinculo> getTipoVinculoList() {
        return tipoVinculoList;
    }

    public void setTipoVinculoList(List<TipoVinculo> tipoVinculoList) {
        this.tipoVinculoList = tipoVinculoList;
    }

    public List<Servidor> getServidorList() {
        return servidorList;
    }

    public void setServidorList(List<Servidor> servidorList) {
        this.servidorList = servidorList;
    }

    public List<Pessoa> getPessoaList() {
        return pessoaList;
    }

    public void setPessoaList(List<Pessoa> pessoaList) {
        this.pessoaList = pessoaList;
    }
}
