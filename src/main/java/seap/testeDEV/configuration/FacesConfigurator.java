package seap.testeDEV.configuration;

import org.ocpsoft.rewrite.servlet.RewriteFilter;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import seap.testeDEV.util.ViewScope;

import javax.faces.webapp.FacesServlet;
import javax.servlet.DispatcherType;

@Configuration
public class FacesConfigurator {

    @Bean
    public FacesServlet facesServlet() {
        return new FacesServlet();
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(facesServlet(), "*.jsf");
        servletRegistrationBean.setName("FacesServlet");
        return servletRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean rewriteFilter() {
        FilterRegistrationBean rewriteFilter = new FilterRegistrationBean(new RewriteFilter());
        rewriteFilter.setDispatcherTypes(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.ASYNC, DispatcherType.ERROR);
        rewriteFilter.addUrlPatterns("/*");
        return rewriteFilter;
    }


    @Bean
    public BeanFactoryPostProcessor beanFactoryPostProcessor() {
        return configurableListableBeanFactory -> configurableListableBeanFactory.registerScope("view", new ViewScope());
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return servletContext -> {
            servletContext.setInitParameter("javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL", "true");
            servletContext.setInitParameter("javax.faces.VALIDATE_EMPTY_FIELDS", "false");
            servletContext.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
            servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
            servletContext.setInitParameter("javax.faces.FACELETS_REFRESH_PERIOD", "2");
            servletContext.setInitParameter("javax.faces.PARTIAL_STATE_SAVING_METHOD", "true");
            /*servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Production");*/
            servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
            servletContext.setInitParameter("facelets.DEVELOPMENT", "true");
            servletContext.setInitParameter("primefaces.UPLOADER", "native");
            //servletContext.setInitParameter("primefaces.THEME", "ultima-dark-blue");
            servletContext.setInitParameter("primefaces.FONT_AWESOME", "true");
        };
    }
}