package seap.testeDEV.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table (name = "TD03_SERVIDOR")
@SequenceGenerator(name = "TD03_SERVIDOR_SEQ", sequenceName = "TD03_SERVIDOR_SEQ", allocationSize = 1)
public class Servidor implements Serializable {
    private static final long serialVersiouUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TD03_SERVIDOR_SEQ")
    @Column(name = "TD03_COD_SERVIDOR")
    private Long id;

    @Column(name = "TD03_MATRICULA")
    private String matricula;

    @OneToOne
    @JoinColumn(name = "FKTD03TD01_COD_PESSOA")
    private Pessoa pessoa;

    @OneToOne
    @JoinColumn(name = "FKTD03TD04_COD_TIPO_VINCULO")
    private TipoVinculo tipoVinculo;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public TipoVinculo getTipoVinculo() {
        return tipoVinculo;
    }

    public void setTipoVinculo(TipoVinculo tipoVinculo) {
        this.tipoVinculo = tipoVinculo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servidor servidor = (Servidor) o;
        return id == servidor.id &&
                Objects.equals(matricula, servidor.matricula) &&
                Objects.equals(pessoa, servidor.pessoa) &&
                Objects.equals(tipoVinculo, servidor.tipoVinculo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, matricula, pessoa, tipoVinculo);
    }

    @Override
    public String toString() {
        return "Servidor{" +
                "id=" + id +
                ", matricula='" + matricula + '\'' +
                ", pessoa=" + pessoa +
                ", tipoVinculo=" + tipoVinculo +
                '}';
    }
}
