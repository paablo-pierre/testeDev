package seap.testeDEV.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table (name = "TD04_TIPO_VINCULO")
@SequenceGenerator(name = "TD04_TIPO_VINCULO_SEQ", sequenceName = "TD04_TIPO_VINCULO_SEQ", allocationSize = 1)
public class TipoVinculo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TD04_TIPO_VINCULO_SEQ")
    @Column (name = "TD04_COD_TIPO_VINCULO")
    private Integer Id;

    @Column (name = "TD04_DESCRICAO")
    private String descricao;

    public TipoVinculo() {

    }

    public TipoVinculo(Integer id) {
        this.Id = id;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(Servidor descricao) {
        descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoVinculo that = (TipoVinculo) o;
        return Id == that.Id &&
                Objects.equals(descricao, that.descricao);
    }

    @Override
    public int hashCode() {

        return Objects.hash(Id, descricao);
    }

    @Override
    public String toString() {
        return "TipoVinculo{" +
                "Id=" + Id +
                ", Descricao='" + descricao + '\'' +
                '}';
    }
}
