package seap.testeDEV.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import seap.testeDEV.model.Telefone;

public interface TelefoneRepository extends JpaRepository<Telefone, Long> {

}
