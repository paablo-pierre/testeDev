package seap.testeDEV.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import seap.testeDEV.model.Pessoa;

import java.util.List;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    /* Busca todas as pessoas pelo nome
     * utilizando uma @Query
     */
    @Query("SELECT p from Pessoa p WHERE p.nome LIKE %:nome%")
    List<Pessoa> findAllByNomeIsLike(@Param("nome") String nome);

}