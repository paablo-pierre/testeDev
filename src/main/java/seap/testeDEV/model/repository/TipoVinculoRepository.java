package seap.testeDEV.model.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import seap.testeDEV.model.TipoVinculo;

@Repository
public interface TipoVinculoRepository extends JpaRepository<TipoVinculo, Integer> {


}
