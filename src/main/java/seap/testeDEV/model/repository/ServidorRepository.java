package seap.testeDEV.model.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import seap.testeDEV.model.Pessoa;
import seap.testeDEV.model.Servidor;
import seap.testeDEV.model.TipoVinculo;

import java.util.List;

@Repository
public interface ServidorRepository extends JpaRepository<Servidor, Long> {
    List<Servidor> findAllByTipoVinculo(TipoVinculo tipoVinculo);

    @Query("SELECT s FROM Servidor s JOIN s.pessoa p WHERE p.nome LIKE %:nome%")
    List<Servidor> findAllByPessoaNome(@Param("nome") String nome);

    Servidor findFirstByPessoa(Pessoa pessoa);

    @Query("SELECT s FROM Servidor s JOIN s.pessoa p JOIN s.tipoVinculo t WHERE  p.nome LIKE %:nome% AND t.id = :tipovinculoId")
    List<Servidor> findAllByPessoaNomeAndTipoVinculo(@Param("nome") String nome, @Param("tipovinculoId") Integer tipovinculoId);


}
