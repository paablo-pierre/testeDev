package seap.testeDEV.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "TD01_PESSOA")
@SequenceGenerator(name = "TD01_PESSOA_SEQ", sequenceName = "TD01_PESSOA_SEQ", allocationSize = 1)
public class Pessoa implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TD01_PESSOA_SEQ")
    @Column (name = "TD01_COD_PESSOA")
    private Long codPessoa;

    @Column (name = "TD01_NOME")
    private String nome;

    @Column (name = "TD01_CPF")
    private String cpf;

    public Long getCodPessoa() {
        return codPessoa;
    }

    public void setCodPessoa(Long codPessoa) {
        this.codPessoa = codPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa pessoa = (Pessoa) o;
        return Objects.equals(codPessoa, pessoa.codPessoa) &&
                Objects.equals(nome, pessoa.nome) &&
                Objects.equals(cpf, pessoa.cpf);
    }

    @Override
    public int hashCode() {

        return Objects.hash(codPessoa, nome, cpf);
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "codPessoa=" + codPessoa +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                '}';
    }
}