package seap.testeDEV.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table (name = "TD02_TELEFONE_PESSOA")
@SequenceGenerator(name = "TD02_TELEFONE_PESSOA_SEQ", sequenceName = "TD02_TELEFONE_PESSOA_SEQ", allocationSize = 1)
public class Telefone {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TD02_TELEFONE_PESSOA_SEQ")
    @Column (name = "TD02_COD_TELEFONE_PESSOA")
    private Long Id;

    @Column (name = "TD02_NUMERO")
    private String numero;

    @Column (name = "TD02_DDD")
    private String DDD;


    @OneToOne
    @JoinColumn (name = "FKTD02TD01_COD_PESSOA")
    private Pessoa pessoa;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDDD() {
        return DDD;
    }

    public void setDDD(String DDD) {
        this.DDD = DDD;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Telefone telefone = (Telefone) o;
        return Objects.equals(Id, telefone.Id) &&
                Objects.equals(numero, telefone.numero) &&
                Objects.equals(DDD, telefone.DDD) &&
                Objects.equals(pessoa, telefone.pessoa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(Id, numero, DDD, pessoa);
    }

    @Override
    public String toString() {
        return "Telefone{" +
                "Id=" + Id +
                ", numero='" + numero + '\'' +
                ", DDD='" + DDD + '\'' +
                ", pessoa=" + pessoa +
                '}';
    }
}
